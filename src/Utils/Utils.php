<?php

namespace App\Utils;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class Utils
{
    private PHPMailer $mailer;

    /**
     * Utils constructor
     */
    public function __construct()
    {
        $this->mailer = new PHPMailer(true);
    }


    /**
     * Function used to format custom dump
     * @param $toDump
     */
    public function dump($toDump){
        echo "<pre>";
        var_dump($toDump);
        echo "</pre>";
    }

    /**
     * This function is used to display flash message.
     */
    public
    function alert() {
        if (isset($_SESSION['flash'])):?>
            <?php foreach ($_SESSION['flash'] as $type => $message): ?>
                <?php $class = ''; ?>
                <?php if (isset($_SESSION['flash']['success'])) {
                    $class = 'toast-success';
                } ?>
                <?php if (isset($_SESSION['flash']['error'])) {
                    $class = 'toast-error';
                } ?>
                <script type='text/javascript'> setTimeout(function () {
                        M.toast({html: "<?= $message; ?>", classes: '<?= $class ?>'});
                    }, 200)</script>
            <?php endforeach; ?>
            <?php unset($_SESSION['flash']); ?>
        <?php endif;
    }

    /**
     * This function is used to create a random string.
     * @param $length
     * @return bool|string
     */
    public function str_random($length) {
        $alphabet = '0123456789qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM';
        return substr(str_shuffle(str_repeat($alphabet, $length)), 0, $length);
    }


    /**
     * Function used to send the validation email to activate user account
     * @param $receiver
     * @param $receiverName
     * @param $token
     */
    public function registerMail($receiver,$receiverName,$token){
        try {
            $this->mailer->setFrom('no-reply@myword.com', 'MyWord');
            $this->mailer->addAddress($receiver, $receiverName);
            $this->mailer->addReplyTo('no-reply@myword.com', 'MyWord');

            $this->mailer->Subject = 'MyWord - Valider votre compte';
            $this->mailer->Body = 'Bienvenue ' .$receiverName. ' sur MyWord, valider votre compte en cliquant sur: http://' . $_SERVER["HTTP_HOST"] . '/validate/'.$token;
            $this->mailer->AltBody = 'Bienvenue ' .$receiverName. ' sur MyWord, valider votre compte en cliquant sur: http://' . $_SERVER["HTTP_HOST"] . '/validate/'.$token;

            $this->mailer->send();

        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$this->mailer->ErrorInfo}";
        }
    }

    public function forgotPasswordMail($receiver,$receiverName,$token){
        try {
            $this->mailer->setFrom('no-reply@myword.com', 'MyWord');
            $this->mailer->addAddress($receiver, $receiverName);
            $this->mailer->addReplyTo('no-reply@myword.com', 'MyWord');

            $this->mailer->Subject = 'MyWord - Réinitialiser votre mot de passe';
            $this->mailer->Body = 'Bonjour '.$receiverName.', une réinitialisation de votre mot de passe à été demandé, cliqué sur ce lien pour le réinitialiser: http://' . $_SERVER["HTTP_HOST"] . '/reset/password/'.$token;
            $this->mailer->AltBody = 'Bonjour '.$receiverName.', une réinitialisation de votre mot de passe à été demandé, cliqué sur ce lien pour le réinitialiser: http://' . $_SERVER["HTTP_HOST"] . '/reset/password/'.$token;

            $this->mailer->send();

        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$this->mailer->ErrorInfo}";
        }
    }



}