<?php

namespace App\User;

use App\Database\Database;
use App\Utils\Utils;
use DateTime;
use Exception;

class UserManager
{
    private Database $db;
    private Utils $utils;

    /**
     * UserManager constructor
     */
    public function __construct()
    {
        $this->db = new Database();
        $this->utils = new Utils();
    }

    /**
     * This function is used to find and get object of an user
     * @param $id
     * @return User|bool
     */

    public function findById($id){
        $user = $this->db->prepareReturn('SELECT * FROM users WHERE id= ?', [$id]);
        if (!$user) return false;
        return new User($user->id, $user->username,$user->email,$user->password,$user->birthdate,$user->validate,$user->registerDate,$user->validateToken,$user->retrievePasswordToken,$user->attempt);
    }

    /**
     * This function is used to find and get object of an user
     * @param $username
     * @return User|bool
     */
    public function findByUsername($username)
    {
        $user = $this->db->prepareReturn('SELECT * FROM users WHERE username= ?', [$username]);
        if (!$user) return false;
        return new User($user->id, $user->username,$user->email,$user->password,$user->birthdate,$user->validate,$user->registerDate,$user->validateToken,$user->retrievePasswordToken,$user->attempt);
    }

    /**
     * This function is used to find and get object of an user
     * @param $filter
     * @param $value
     * @return User|bool
     */
    public function findBy($filter,$value)
    {
        $user = $this->db->prepareReturn('SELECT * FROM users WHERE '.$filter.'= ?', [$value]);
        if (!$user) return false;
        return new User($user->id, $user->username,$user->email,$user->password,$user->birthdate,$user->validate,$user->registerDate,$user->validateToken,$user->retrievePasswordToken,$user->attempt);
    }


    /**
     * Function used to login an user.
     * @param $data
     */
    public function login($data){
        $username = $data["username"];
        $password = $data["password"];

        $user = $this->findByUsername($username);

        if (!$user){
            $arrErrors["username"] = "Utilisateur inexistant !";

            header('HTTP/1.1 400 Bad Request');
            echo json_encode([
                "error" => true,
                "message" => [
                    "username" => "Utilisateur innexistant !"
                ]
            ]);
        }else{

            if ($user->getValidate() === 1){

                if ($user->getAttempt() < 3){
                    if (password_verify($password,$user->getPassword())){

                        $_SESSION["auth"] = [
                            "id" => $user->getId(),
                            "username" => $user->getUsername()
                        ];

                        $_SESSION["flash"]["success"] = "Vous êtes connecté !";

                        $user->setAttempt(0);
                        $user->update();

                        echo json_encode([
                            "error" => false,
                            "message" => [
                                "connect" => "Connecté !"
                            ]
                        ]);
                    }else{
                        header('HTTP/1.1 400 Bad Request');
                        echo json_encode([
                            "error" => true,
                            "message" => [
                                "password" => "Mauvais mot de passe !"
                            ]
                        ]);

                        $calcAttempt = $user->getAttempt() + 1;
                        $user->setAttempt($calcAttempt);
                        $user->update();
                    }
                }else{
                    header('HTTP/1.1 400 Bad Request');
                    echo json_encode([
                        "error" => true,
                        "message" => [
                            "connect" => "Compte bloqué trop d'essaies !"
                        ]
                    ]);
                }
            }else{
                header('HTTP/1.1 400 Bad Request');
                echo json_encode([
                    "error" => true,
                    "message" => [
                        "connect" => "Compte pas encore validé !"
                    ]
                ]);
            }
        }
    }

    /**
     * Function used to register an user.
     * @param $data
     * @throws Exception
     */
    public function register($data){
        $username = $data["username"];
        $email = $data["email"];
        $birthdate = new DateTime($data["birthdate"]);
        $birthdateFormat = $birthdate->format('d.m.Y');
        $password = $data["password"];
        $passwordConfirm = $data["passwordConfirm"];
        $validateToken = $this->utils->str_random(15).strval(time());

        $arrErrors = [];


        if (!$this->findByUsername($username)){

            if(!$this->findBy("email",$email)){

                // Check field if correct
                foreach ($data as $k => $v) {

                    if ($k === "email"){
                        if (!filter_var($v,FILTER_VALIDATE_EMAIL)){
                            $arrErrors[$k] = "Email incorrect !";
                        }
                    }

                    if ($k === "birthdate"){
                        if (!preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/",$birthdateFormat)){
                            $arrErrors[$k] = "Date de naissance incorrect !";
                        }
                    }

                    if (empty($v)) {
                        $arrErrors[$k] = "Veuillez remplir le champs correctement !";
                    }
                }

                //Check if password are equals
                if ($passwordConfirm !== $password){
                    $arrErrors["password"] = "Les mots de passe ne correspondent pas !";
                    $arrErrors["passwordConfirm"] = "Les mots de passe ne correspondent pas !";
                }else{
                    $passHash = password_hash($password,PASSWORD_BCRYPT);
                }

                //Check if there is errors
                if (count($arrErrors) > 0){
                    //Errors detected returns to user
                    header('HTTP/1.1 400 Bad Request');
                    echo json_encode([
                        "error" => true,
                        "message" => $arrErrors
                    ]);
                }else{
                    //Register user
                    $this->db->querySimplePrepare("INSERT INTO users SET username= ?, email=?, password=?,birthdate=?,validateToken=?", [
                        $username,
                        $email,
                        $passHash,
                        $birthdateFormat,
                        $validateToken
                    ]);


                    $this->utils->registerMail($email,$username,$validateToken);



                    $_SESSION["flash"]["success"] = "Un email vous à été envoyé pour valider votre compte !";

                    echo json_encode([
                        "error" => false,
                        "message" => "Inscription réussi"
                    ]);
                }
            }else{
                //Errors detected returns to user
                header('HTTP/1.1 400 Bad Request');
                echo json_encode([
                    "error" => true,
                    "message" => [
                        "email" => "Email déjà utilisée !"
                    ]
                ]);
            }

        }else{
            //user exist
            header('HTTP/1.1 400 Bad Request');
            echo json_encode([
                "error" => true,
                "message" => [
                    "username" => "Utilisateur déjà existant !"
                ]
            ]);
        }
    }

    /**
     * Function used to validate user account with email
     * @param $data
     */
    public function validateAccount($data){
        $user = $this->findBy("validateToken",$data["token"]);

        if ($user){
            $user->setValidateToken(NULL);
            $user->setValidate(1);
            $user->update();

            $_SESSION["flash"]["success"] = "Compte validé, tu peux te connecter !";
            header("Location: /login");
        }{
            header("Location: /login");
        }
    }


    /**
     * This function is used to logut the user.
     */
    public function logout() {
        session_unset();
        session_destroy();
        unset($_SESSION['auth']);
        session_start();
        $_SESSION['flash']['success'] = "Vous êtes bien déconnecté !";
        header('location: /');
        exit;
    }


    public function forgotPassword($data){
        $user = $this->findBy("email",$data["emailPasswordReset"]);
        $retrievePasswordToken = $this->utils->str_random(15).strval(time());


        if ($user){
            $this->utils->forgotPasswordMail($user->getEmail(),$user->getUsername(),$retrievePasswordToken);
            $user->setRetrievePasswordToken($retrievePasswordToken);
            $user->update();
            $_SESSION["flash"]["success"] = "Un email de réinitialisation à été envoyé !";

            header('Location: /login');
        }else{
            $_SESSION["flash"]["error"] = "Aucun compte lié à cet email !";
            header('Location: /login');
        }
    }

    public function resetPassword($data){
        $token = $data["token"];
        $password = $data["password"];
        $confirmPassword = $data["confirmPassword"];

        $arrErrors = [];

        $user = $this->findBy("retrievePasswordToken",$token);

        if ($user){
            foreach ($_POST as $k => $v){
                if (empty($v)){
                    $arrErrors[$k] = "Veuillez remplir tout les champs !";
                }
            }

            if ($password === $confirmPassword){
                $user->setPassword(password_hash($password,PASSWORD_BCRYPT));
                $user->setRetrievePasswordToken(NULL);
                $user->update();
                $_SESSION["flash"]["success"] = "Votre mot de passe est changé !";
            }else{
                $arrErrors["password"] = "Les mots de passe ne correspondent pas !";
                $arrErrors["confirmPassword"] = "Les mots de passe ne correspondent pas !";
            }
        }else{
            $arrErrors["error"] = "Utilisateur innexistant !";
        }

        if (count($arrErrors) > 0){
            header('HTTP/1.1 400 Bad Request');
            echo json_encode([
                "error" => true,
                "message" => $arrErrors
            ]);
        }else{
            echo json_encode([
                "error" => false,
                "message" => "Mot de passe changé !"
            ]);
        }
    }
}