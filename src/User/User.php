<?php

namespace App\User;

use App\Database\Database;

class User
{
    private int $id;
    private string $username;
    private string $email;
    private string $password;
    private string $birthdate;
    private int $validate;
    private string $registerDate;
    private ?string $validateToken;
    private ?string $retrievePasswordToken;
    private int $attempt;

    private Database $db;

    /**
     * @param int $id
     * @param string $username
     * @param string $email
     * @param string $password
     * @param string $birthdate
     * @param int $validate
     * @param string $registerDate
     * @param string|null $validateToken
     * @param string|null $retrievePasswordToken
     */
    public function __construct(int $id, string $username, string $email, string $password, string $birthdate, int $validate, string $registerDate, ?string $validateToken, ?string $retrievePasswordToken, int $attempt)
    {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->birthdate = $birthdate;
        $this->validate = $validate;
        $this->registerDate = $registerDate;
        $this->validateToken = $validateToken;
        $this->retrievePasswordToken = $retrievePasswordToken;
        $this->attempt = $attempt;

        $this->db = new Database();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getBirthdate(): string
    {
        return $this->birthdate;
    }

    /**
     * @param string $birthdate
     */
    public function setBirthdate(string $birthdate): void
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return int
     */
    public function getValidate(): int
    {
        return $this->validate;
    }

    /**
     * @param int $validate
     */
    public function setValidate(int $validate): void
    {
        $this->validate = $validate;
    }

    /**
     * @return string
     */
    public function getRegisterDate(): string
    {
        return $this->registerDate;
    }

    /**
     * @param string $registerDate
     */
    public function setRegisterDate(string $registerDate): void
    {
        $this->registerDate = $registerDate;
    }

    /**
     * @return string
     */
    public function getValidateToken() : ?string
    {
        return $this->validateToken;
    }

    /**
     * @param string|null $validateToken
     */
    public function setValidateToken(?string $validateToken): void
    {
        $this->validateToken = $validateToken;
    }

    /**
     * @return string
     */
    public function getRetrievePasswordToken(): ?string
    {
        return $this->retrievePasswordToken;
    }

    /**
     * @param string|null $retrievePasswordToken
     */
    public function setRetrievePasswordToken(?string $retrievePasswordToken): void
    {
        $this->retrievePasswordToken = $retrievePasswordToken;
    }

    /**
     * @return int
     */
    public function getAttempt(): int
    {
        return $this->attempt;
    }

    /**
     * @param int $attempt
     */
    public function setAttempt(int $attempt): void
    {
        $this->attempt = $attempt;
    }

    /**
     * This function is used to update the user in the database trought the object
     */
    public function update(){
        $this->db->querySimplePrepare('UPDATE users SET username=?, email=?,password=?,birthdate=?,validate=?,registerDate=?,validateToken=?,retrievePasswordToken=?,attempt=? WHERE id=?',[
            $this->username,
            $this->email,
            $this->password,
            $this->birthdate,
            $this->validate,
            $this->registerDate,
            $this->validateToken,
            $this->retrievePasswordToken,
            $this->attempt,
            $this->id,
        ]);
    }

    /**
     * This function is used to delete an user
     */
    public function delete(){
        $this->db->querySimplePrepare('DELETE FROM users WHERE id =?', [
            $this->id
        ]);
    }

}