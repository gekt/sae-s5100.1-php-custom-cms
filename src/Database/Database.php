<?php
namespace App\Database;

use PDO;
use PDOStatement;

class Database {
    public string $db_name;
    public string $db_user;
    public string $db_pass;
    public string $db_host;
    public $pdo;

    /**
     * DB constructor.
     */
    public function __construct() {
        $this->db_name = "myword";
        $this->db_user = "root";
        $this->db_pass = "";
        $this->db_host = "127.0.0.1";
    }

    /**
     * Initialize PDO connection
     * @return PDO
     */
    private function getPDO(): PDO
    {
        if ($this->pdo === null) {
            $pdo = new PDO('mysql:dbname=' . $this->db_name . ';host=' . $this->db_host . '', $this->db_user, $this->db_pass);

            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $pdo->exec('SET NAMES utf8');

            $this->pdo = $pdo;
        }
        return $this->pdo;
    }

    /**
     * Execute simple query with prepared statement.
     * @param $statement
     * @param array $attributes
     * @return PDOStatement
     */
    public function querySimplePrepare($statement, array $attributes = []): PDOStatement
    {
        $req = $this->getPDO()->prepare($statement);
        $req->execute($attributes);
        return $req;
    }


    /**
     * Execute prepared query with return object.
     * @param $statement
     * @param array $attributes
     * @return bool|mixed
     */
    public function prepareReturn($statement, array $attributes = []) {
        try {
            $req = $this->getPDO()->prepare($statement);
            $req->execute($attributes);
            return $req->fetch(PDO::FETCH_OBJ);
        } catch (\PDOException $e) {
            echo $e;
            return false;
        }
    }

}
