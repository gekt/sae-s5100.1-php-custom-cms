<?php

namespace App\TwigExtension;

use App\User\User;
use App\User\UserManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    private UserManager $userManager;

    /**
     * TwigExtension constructor
     */
    public function __construct()
    {
        $this->userManager = new UserManager();
    }

    /**
     * Register custom function to twig
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('isOnline', [$this, 'isOnline']),
            new TwigFunction('user', [$this, 'user']),
        ];
    }

    /**
     * Function used to check if user is connected
     * @return bool
     */
    public function isOnline()
    {
        if (isset($_SESSION["auth"])){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Function used to retrieve the user through twig
     * @return User|bool|void
     */
    public function user()
    {
        if ($this->isOnline()){
            return $this->userManager->findById($_SESSION["auth"]["id"]);
        }
    }
}