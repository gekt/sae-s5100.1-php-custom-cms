<?php

namespace App\Topic;

use App\Database\Database;

class Topic
{
    private int $id;
    private string $title;
    private string $content;
    private string $createdAt;
    private int $authorId;

    private Database $db;

    /**
     * @param int $id
     * @param string $title
     * @param string $content
     * @param string $createdAt
     * @param int $authorId
     */
    public function __construct(int $id, string $title, string $content, string $createdAt, int $authorId)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->createdAt = $createdAt;
        $this->authorId = $authorId;

        $this->db = new Database();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     */
    public function setAuthorId(int $authorId): void
    {
        $this->authorId = $authorId;
    }

    /**
     * This function is used to update the topic in the database trought the object
     */
    public function update(){
        $this->db->querySimplePrepare('UPDATE topics SET title=?,content=?,createdAt=?,authorId=? WHERE id =?',[
            $this->title,
            $this->content,
            $this->createdAt,
            $this->authorId,
            $this->id,
        ]);
    }

    /**
     * This function is used to delete a topic
     */
    public function delete(){
        $this->db->querySimplePrepare('DELETE FROM topics WHERE id =?', [
            $this->id
        ]);

        $this->db->querySimplePrepare("DELETE FROM topicanswer WHERE topicId = ?",[
            $this->id
        ]);
    }

}