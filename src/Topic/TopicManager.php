<?php

namespace App\Topic;

use App\Database\Database;
use App\User\UserManager;
use Exception;

class TopicManager
{

    private Database $db;
    private UserManager $userManager;

    /**
     * Constructor of TopicManager
     */
    function __construct()
    {
        $this->db = new Database();
        $this->userManager = new UserManager();
    }

    /**
     * Function used to get a topic from database
     * @param $filter
     * @param $value
     * @return Topic|false
     */
    public function findBy($filter,$value){
        $topic = $this->db->prepareReturn('SELECT * FROM topics WHERE '.$filter.'= ?', [$value]);
        if (!$topic) return false;
        return new Topic($topic->id, $topic->title,$topic->content,$topic->createdAt,$topic->authorId);
    }

    /**
     * Function used to create a topic
     * @throws Exception
     */
    public function createTopic($data){
        $arrErrors = [];

        $authorId = $_SESSION["auth"]["id"];
        $title = $data["title"];
        $content = $data["content"];

        if (empty($title)){
            $arrErrors["title"] = "Veuillez entrer un titre !";
        }

        if ($content === "<p><br></p>"){
            $arrErrors["content"] = "Veuillez écrire quelque chose !";
        }

        if (strlen($title) > 120){
            $arrErrors["title"] = "Titre trop long !";
        }

        if (count($arrErrors) > 0){
            header('HTTP/1.1 400 Bad Request');
            echo json_encode([
                "error" => true,
                "message" => $arrErrors
            ]);
        }else{
            $this->db->querySimplePrepare("INSERT INTO topics SET title=?, content=?, authorId=?",[
                $title,
                $content,
                $authorId
            ]);

            $_SESSION["flash"]["success"] = "Topic crée !";

            echo json_encode([
                "error" => false,
                "message" => "Topic crée !"
            ]);
        }
    }

    /**
     * Function used to edit a topic
     * @param $data
     */
    public function editTopic($data){
        $arrErrors = [];

        $title = $data["title"];
        $content = $data["content"];
        $id = $data["id"];

        $topic = $this->findBy("id",$id);

        if ($topic){

            if (empty($title)){
                $arrErrors["title"] = "Veuillez entrer un titre !";
            }

            if ($content === "<p><br></p>"){
                $arrErrors["content"] = "Veuillez écrire quelque chose !";
            }

            if ($topic->getAuthorId() === $_SESSION["auth"]["id"]){
                $topic->setTitle($title);
                $topic->setContent($content);
                $topic->update();

                $_SESSION["flash"]["success"] = "Topic modifié !";
            }
        }else{
            echo json_encode([
                "error" => true,
                "message" => "topic innexistant !"
            ]);
        }

        if (count($arrErrors) > 0){
            header('HTTP/1.1 400 Bad Request');
            echo json_encode([
                "error" => true,
                "message" => $arrErrors
            ]);
        }else{
            echo json_encode([
                "error" => false,
                "message" => "Topic modifié"
            ]);
        }

    }

    /**
     * Function used to delete a topic
     * @param $data
     */
    public function deleteTopic($data){
        $arrErrors = [];
        $topic = $this->findBy("id",$data["id"]);

        if ($topic){
            if ($topic->getAuthorId() === $_SESSION["auth"]["id"]){
                $topic->delete();
                $_SESSION["flash"]["success"] = "Topic supprimé !";
            }
        }else{
            $arrErrors["deleteError"] = "Topic innexistant !";
        }

        if (count($arrErrors) > 0){
            header('HTTP/1.1 400 Bad Request');
            echo json_encode([
                "error" => true,
                "message" => $arrErrors
            ]);
        }else{
            echo json_encode([
                "error" => false,
                "message" => "topic supprimé !"
            ]);
        }


    }

    /**
     * Function used to have the list of all topics
     * @return array
     */
    public function getTopicList(): array
    {
        $arrTopics = [];

        //$req = $this->db->querySimple("SELECT * FROM topics ORDER BY id DESC ");
        $req = $this->db->querySimplePrepare("SELECT * FROM topics ORDER BY id DESC ");

        while ($d = $req->fetch(\PDO::FETCH_OBJ)){
            $arrTopics[] = [
                "id" => $d->id,
                "author" => $this->userManager->findBy("id",$d->authorId)->getUsername(),
                "title" => $d->title,
                "content" => $d->content
            ];
        }

        return $arrTopics;
    }

    /**
     * Function used to search a topic
     * @param $data
     * @return array
     */

    public function searchTopic($data): array
    {
        $arrTopics = [];
        $like = "%".$data["search"]."%";

        $req = $this->db->querySimplePrepare("SELECT * FROM topics WHERE title LIKE ? ",[
            $like
        ]);

        while ($d = $req->fetch(\PDO::FETCH_OBJ)){
            $arrTopics[] = [
                "id" => $d->id,
                "author" => $this->userManager->findBy("id",$d->authorId)->getUsername(),
                "title" => $d->title,
                "content" => $d->content
            ];
        }

        return $arrTopics;
    }

    /**
     * Function used to find a topic
     * @param $data
     * @return Topic|void
     */
    public function findTopic($data){
        $topic = $this->findBy("id",$data["id"]);

        if ($topic){
            return $topic;
        }else{
            $_SESSION["flash"]["success"] = "Aucun topic trouvé !";
            header("Location: /");
        }
    }

}

