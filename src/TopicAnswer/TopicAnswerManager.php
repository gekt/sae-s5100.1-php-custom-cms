<?php

namespace App\TopicAnswer;

use App\Database\Database;
use App\User\UserManager;

class TopicAnswerManager
{
    private Database $db;
    private UserManager $userManager;

    /**
     * Constructor of TopicAnswerManager
     */
    public function __construct()
    {
        $this->db = new Database();
        $this->userManager = new UserManager();
    }

    /**
     * Function used to get an answer from database
     * @param $filter
     * @param $value
     * @return TopicAnswer|false
     */
    public function findBy($filter,$value){
        $topicAnswer = $this->db->prepareReturn('SELECT * FROM topicanswer WHERE '.$filter.'= ?', [$value]);
        if (!$topicAnswer) return false;
        return new TopicAnswer($topicAnswer->id, $topicAnswer->content,$topicAnswer->authorId,$topicAnswer->topicId,$topicAnswer->createdAt);
    }

    /**
     * Function used to create an answer
     * @param $data
     */
    public function createAnswer($data){
        $topicId = $data["topicId"];
        $content = $data["content"];

        $arrErrors = [];

        if ($content !== "<p><br></p>"){
            $this->db->querySimplePrepare("INSERT INTO topicanswer SET content = ?, authorId = ?, topicId = ?", [
               $content,
               $_SESSION["auth"]["id"],
               $topicId
            ]);
            $_SESSION["flash"]["success"] = "Réponse postée !";
        }else{
            $arrErrors[] = "Vous devez écrire quelque chose !";
        }

        if (count($arrErrors) > 0){
            header('HTTP/1.1 400 Bad Request');
            echo json_encode([
                "error" => true,
                "message" => $arrErrors
            ]);
        }else{
            echo json_encode([
                "error" => false,
                "message" => "Réponse postée !"
            ]);
        }
    }

    /**
     * Function used to delete an answer
     * @param $data
     */
    public function deleteAnswer($data){
        $arrErrors = [];
        $answerId = $data["id"];
        $answer = $this->findBy("id",$answerId);

        if ($answer){
            if ($answer->getAuthorId() === $_SESSION["auth"]["id"]){
                $answer->delete();
                $_SESSION["flash"]["success"] = "Réponse supprimée !";
            }
        }else{
            $arrErrors["error"] = "Réponse innexistante !";
        }

        if (count($arrErrors) > 0){
            header('HTTP/1.1 400 Bad Request');
            echo json_encode([
                "error" => true,
                "message" => $arrErrors
            ]);
        }else{
            echo json_encode([
                "error" => false,
                "message" => "Réponse supprimée !"
            ]);
        }
    }

    /**
     * Function used to get a list of answer for the topic
     * @param $data
     * @return array
     */
    public function getAnswersList($data): array
    {
        $topicId = $data["id"];

        $arrAnswer = [];

        $req = $this->db->querySimplePrepare("SELECT * FROM topicanswer WHERE topicId = ?",[$topicId]);

        while ($d = $req->fetch(\PDO::FETCH_OBJ)){
            $arrAnswer[] = [
                "id" => $d->id,
                "author" => $this->userManager->findBy("id",$d->authorId)->getUsername(),
                "authorId" => $d->authorId,
                "content" => $d->content,
                "createdAt" => $d->createdAt
            ];
        }

        return $arrAnswer;

    }

}