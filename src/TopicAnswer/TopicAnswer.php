<?php

namespace App\TopicAnswer;

use App\Database\Database;

class TopicAnswer
{
    private int $id;
    private string $content;
    private int $authorId;
    private int $topicId;
    private string $createdAt;


    private Database $db;

    /**
     * @param int $id
     * @param string $content
     * @param int $authorId
     * @param int $topicId
     */
    public function __construct(int $id, string $content, int $authorId, int $topicId, string $createdAt)
    {
        $this->id = $id;
        $this->content = $content;
        $this->authorId = $authorId;
        $this->topicId = $topicId;
        $this->createdAt = $createdAt;

        $this->db = new Database();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     */
    public function setAuthorId(int $authorId): void
    {
        $this->authorId = $authorId;
    }

    /**
     * @return int
     */
    public function getTopicId(): int
    {
        return $this->topicId;
    }

    /**
     * @param int $topicId
     */
    public function setTopicId(int $topicId): void
    {
        $this->topicId = $topicId;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * This function is used to update the topic in the database trought the object
     */
    public function update(){
        $this->db->querySimplePrepare('UPDATE topicanswer SET content=?,authorId=?,topicId=?, createdAt=? WHERE id =?',[
            $this->content,
            $this->authorId,
            $this->topicId,
            $this->createdAt,
            $this->id,
        ]);
    }

    /**
     * This function is used to delete a topic
     */
    public function delete(){
        $this->db->querySimplePrepare('DELETE FROM topicanswer WHERE id =?', [
            $this->id
        ]);
    }
}