# What is "MyWord" ?
MyWord is a forum CMS who can be deployed on any servers and developed in PHP.

# What is the dependencies ?
> #### PHP dependencies
> - **Twig**: for the templating system [Link](https://twig.symfony.com/doc/3.x/)
> - **PhpMailer**: to send mails to users [Link](https://github.com/PHPMailer/PHPMailer)
> #### JavaScript dependencies
> - **Quill**: It's a wysiwyg used as editor on "MyWord" [Link](https://quilljs.com/docs/quickstart/)
> #### CSS/JS dependencies
> - **Materialize**: Used for css and some js component [Link](https://materializecss.com/)
# Good to know !
Everything you need to know about "MyWord" is located in a documentation in the "docs/app" folder (phpDocumentor).

# How to set-up?
**Simply run:**
>composer install

**and**

>composer update


**Dont forget to config your database in "src/database"**
```php
$this->db_name = "MyWord"
$this->db_user = "root"
$this->db_pass = "password"
$this->db_host = "localhost"
```

**and dont forget to import the "bdd.sql", locate in the root, into your database to get the correct structure.**


# How the router work?
If you want to add some route this is how the router work:

> ##### POST
 ``` php
     $router->post("/post/register", function (){
        //Call your logic here
     },false); // "false" define if the user has to be connected or not. Can be empty.
 ```
> ##### POST (with args)
 ``` php
     $router->post("/post/register/{id}", function ($args){
        echo $args["id"];
        //Call your logic here
     },false); // "false" define if the user has to be connected or not. Can be empty.
 ```
> ##### GET
 ``` php
    $router->get("/profil", function ($args) use ($twig) {
        echo $twig->render("userProfil.html.twig", [ // Call the twig template
            //Pass some parameter to the twig template
        ]);
    });
 ```
> ##### GET (with args)
 ``` php
    $router->get("/profil/{id}", function ($args) use ($twig) {
        echo $args["id"];
        echo $twig->render("userProfil.html.twig", [ // Call the twig template
            //Pass some parameter to the twig template
        ]);
    });
 ```

> ##### UML
![website uml](./md-uml.png)