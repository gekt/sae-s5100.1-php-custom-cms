<?php

require "../vendor/autoload.php";

session_start();

use App\Router\Router;
use App\Topic\TopicManager;
use App\TopicAnswer\TopicAnswerManager;
use App\User\UserManager;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use App\Utils\Utils;

$router = new Router();

// Specify our Twig templates location
$loader = new FilesystemLoader(__DIR__.'/../templates/');

// Instantiate our Twig
$twig = new Environment($loader);

$twig->addExtension(new \App\TwigExtension\TwigExtension());

//Instantiate Utils
$utils = new Utils();

if ($_SERVER['REQUEST_METHOD'] === "GET"){
    $utils->alert();
}

/**
 * GET ROUTES
 */

//Home
$router->get("/", function () use ($twig){
    $TopicManager = new TopicManager();
    echo $twig->render('home.html.twig', [
        "topics" => $TopicManager->getTopicList()
    ]);
});

//Login
$router->get("/login", function () use ($twig){
    echo $twig->render('login.html.twig');
},false);

///Register
$router->get("/register", function () use ($twig){
    echo $twig->render('register.html.twig');
},false);

//Validate account from email
$router->get("/validate/{token}", function ($args) {
    $userManager = new UserManager();
    $userManager->validateAccount($args);
},false);

//Acount page
$router->get("/account", function () use ($twig) {
    echo $twig->render("profil.html.twig");
},true);

//Logout
$router->get("/logout", function () {
    $userManager = new UserManager();
    $userManager->logout();
},true);

//Create a topic
$router->get("/create/topic", function () use ($twig) {
    echo $twig->render("createTopic.html.twig");
},true);

//Edit a topic
$router->get("/edit/topic/{id}", function ($args) use ($twig) {
    $topicManager = new TopicManager();
    echo $twig->render("editTopic.html.twig", [
        "topic" => $topicManager->findBy("id",$args["id"])
    ]);
},true);

// Reset password
$router->get("/reset/password/{token}", function ($args) use ($twig) {
    $userManager = new UserManager();
    $user = $userManager->findBy("retrievePasswordToken", $args["token"]);

    if ($user){
        echo $twig->render("retrievePassword.html.twig", [
            "token" => $args["token"]
        ]);
    }else{
        header("Location: /login");
    }
},false);

//Search a topic
$router->get("/search/topic/{search}", function ($args) use ($twig) {
    $topicManager = new TopicManager();
    echo $twig->render("searchTopic.html.twig", [
        "topics" => $topicManager->searchTopic($args),
        "search" => $args["search"]
    ]);
});

//Display a topic
$router->get("/topic/{id}", function ($args) use ($twig) {
    $topicManager = new TopicManager();
    $userManager = new UserManager();
    $topicAnswerManager = new TopicAnswerManager();
    echo $twig->render("topic.html.twig", [
        "topic" => $topicManager->findTopic($args),
        "author" => $userManager->findBy("id",$topicManager->findBy("id",$args["id"])->getAuthorId())->getUsername(),
        "answers" => $topicAnswerManager->getAnswersList($args)
    ]);
});


/**
 * POST ROUTES
 */

//Post register
$router->post("/post/register", function (){
    $userManager = new UserManager();
    $userManager->register($_POST);
},false);

//Post login
$router->post("/post/login", function (){
    $userManager = new UserManager();
    $userManager->login($_POST);
},false);

//Post forgot password
$router->post("/post/forgot/password", function (){
    $userManager = new UserManager();
    $userManager->forgotPassword($_POST);
},false);

//Post reset password
$router->post("/post/reset/password", function (){
    $userManager = new UserManager();
    $userManager->resetPassword($_POST);
},false);

//Post create topic
$router->post("/post/create/topic", function (){
    $topicManager = new TopicManager();
    $topicManager->createTopic($_POST);
},true);

//Post edit topic
$router->post("/post/edit/topic", function (){
    $topicManager = new TopicManager();
    $topicManager->editTopic($_POST);
},true);

//Post delete topic
$router->post("/post/delete/topic", function (){
    $topicManager = new TopicManager();
    $topicManager->deleteTopic($_POST);
},true);

//Post answer topic
$router->post("/post/answer/topic", function (){
    $topicManager = new TopicAnswerManager();
    $topicManager->createAnswer($_POST);
},true);

//Post delete answer topic
$router->post("/post/delete/answer", function (){
    $topicManager = new TopicAnswerManager();
    $topicManager->deleteAnswer($_POST);
},true);

//Not found
$router->notFound(function (){
    echo "404";
});
