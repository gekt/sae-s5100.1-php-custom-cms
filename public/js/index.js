/**
 * Search variables
 */


const btnSearch = document.getElementsByClassName("btnSearch");
const navSearch = document.getElementById("secondNav");
const inputSearch = document.getElementById("search");
const formSearch = document.getElementById("searchTopic");

/**
 * Login/Register/reset variables
 */
const formRegister = document.getElementById("registerForm");
const formLogin = document.getElementById("loginForm");
const formRetrievePassword = document.getElementById("resetPasswordForm")

/**
 * Topic variables
 */

const createTopicTitle = document.getElementById("createTitleTopic");
const errorTopicCreateStatus = document.getElementById("errorTopicCreateStatus");

const editTopicTitle = document.getElementById("editTitleTopic");
const editTopicId = document.getElementById("editTopicId")
const errorTopicEditStatus = document.getElementById("errorTopicEditStatus");
let quillEditor;

const deleteTopicTitleModal = document.getElementById("titleModalDelete");
let deleteTopicId;

const deleteAnswerTitle = document.getElementById("titleDeleteAnswerModal");
let deleteAnswerId;


/**
 * Init on document ready
 */
document.addEventListener('DOMContentLoaded', function() {
    let elemsSideNav = document.querySelectorAll('.sidenav');
    M.Sidenav.init(elemsSideNav);

    let elemsModal = document.querySelectorAll('.modal');
    M.Modal.init(elemsModal);

    if (document.getElementById("quillEditor")){

        let toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],

            [{ 'header': 1 }, { 'header': 2 }],               // custom button values
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
            [{ 'direction': 'rtl' }],                         // text direction

            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

            [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            [{ 'font': [] }],
            [{ 'align': [] }],

            ['clean']                                         // remove formatting button
        ];

        quillEditor = new Quill('#quillEditor', {
            modules: {
                toolbar: toolbarOptions
            },
            theme: 'snow'
        });
    }

});

/**
 * Event listener for search
 */
formSearch.addEventListener("submit",(e) => {
    e.preventDefault();
    window.location.href = "/search/topic/"+inputSearch.value;
})

/**
 * Add event listener on search btn
 */
Array.from(btnSearch).forEach((e) => {

    e.addEventListener("click", (e) => {

        let sideNavInstance = M.Sidenav.getInstance(document.getElementById("mobile-demo"));
        sideNavInstance.close();

        navSearch.style = "display:block";
        inputSearch.focus();


    })
})

/**
 * Focus out search display none and reset input
 */
inputSearch.addEventListener("focusout", (e) => {
    e.preventDefault();

    if (navSearch.style.display === "block"){
        navSearch.style = "display:none";
        inputSearch.blur();
        inputSearch.value = "";
    }

})

/**
 * Register event send form
 */
async function registerForm(e){
    e.preventDefault();
    const body = new FormData(formRegister);
    body.append("registerForm","registerForm");

    const errors = document.getElementsByClassName("status-text");

    Array.from(errors).forEach((el) => {
        el.textContent = "";
    });

    const req = await fetch("/post/register", {
        method: "POST",
        headers: {
            "Accept": "application/json"
        },
        body
    })

    const res = await req.json();

    if (req.status === 400){
        Object.keys(res.message).forEach((e) => {
            document.getElementById(e+"-status").textContent = res.message[e];
        })
    }else{
        window.location.href = "/login";
    }
}

/**
 * Login form send
 */
async function loginForm(e){
    e.preventDefault();

    const body = new FormData(formLogin);
    body.append("loginForm", "loginForm");

    const errors = document.getElementsByClassName("status-text");

    Array.from(errors).forEach((el) => {
        el.textContent = "";
    });


    const req = await fetch("/post/login", {
        method: "POST",
        headers: {
            "Accept": "application/json"
        },
        body
    })

    const res = await req.json();

    if (req.status === 400) {
        Object.keys(res.message).forEach((e) => {
            document.getElementById(e + "-status").textContent = res.message[e];
        })
    } else {
        window.location.href = "/";
    }
}

/**
 * reset password form send
 */
async function resetPassword(e){
    e.preventDefault();
    const body = new FormData(formRetrievePassword);
    body.append("formRetrievePassword", "formRetrievePassword");

    const errors = document.getElementsByClassName("status-text");

    Array.from(errors).forEach((el) => {
        el.textContent = "";
    });


    const req = await fetch("/post/reset/password", {
        method: "POST",
        headers: {
            "Accept": "application/json"
        },
        body
    })

    const res = await req.json();

    if (req.status === 400) {
        Object.keys(res.message).forEach((e) => {
            document.getElementById(e + "-status").textContent = res.message[e];
        })
    } else {
        window.location.href = "/login";
    }
}

/**
 * Function used to create a topic
 * @returns {Promise<void>}
 */
async function createTopic(){

    errorTopicCreateStatus.innerHTML = "";

    const title = createTopicTitle.value;
    const content = quillEditor.container.firstChild.innerHTML;

    const body = new FormData();
    body.append("title",title);
    body.append("content", content);

    const req = await fetch("/post/create/topic", {
        method : "POST",
        headers : {
            "Acccept": "application/json"
        },
        body
    })

    const res = await req.json();


    if (req.status === 400) {
        Object.keys(res.message).forEach((e) => {
            let li = document.createElement("li");
            li.className = "red-text";
            li.append(res.message[e])
            errorTopicCreateStatus.append(li);
        })
    } else {
        window.location.href = "/";
    }
}

/**
 * Function used to edit topic
 * @returns {Promise<void>}
 */
async function editTopic(){

    errorTopicEditStatus.innerHTML = "";

    const title = editTopicTitle.value;
    const content = quillEditor.container.firstChild.innerHTML;
    const id = editTopicId.value;

    const body = new FormData();
    body.append("title",title);
    body.append("content", content);
    body.append("id",id)

    const req = await fetch("/post/edit/topic", {
        method : "POST",
        headers : {
            "Acccept": "application/json"
        },
        body
    })

    const res = await req.json();


    if (req.status === 400) {
        Object.keys(res.message).forEach((e) => {
            let li = document.createElement("li");
            li.className = "red-text";
            li.append(res.message[e])
            errorTopicEditStatus.append(li);
        })
    } else {
        window.location.href = "/topic/"+id;
    }
}

/**
 * Function used to set the modal used to delete a topic
 * @param id
 * @param title
 */
function beforeDeleteTopic(id,title){

    deleteTopicTitleModal.textContent = "Supprimer le topic: "+title;
    deleteTopicId = id;
}

/**
 * Function used to delete a topic
 * @returns {Promise<void>}
 */
async function deleteTopic(){
    const body = new FormData();
    body.append("id",deleteTopicId);


    const req = await fetch("/post/delete/topic", {
        method : "POST",
        headers : {
            "Acccept": "application/json"
        },
        body
    })

    const res = await req.json();


    if (req.status === 400) {
        Object.keys(res.message).forEach((e) => {
            M.toast(res.message[e])
        })
    } else {
        window.location.href = "/";
    }
}

/**
 * Function used to create an answer to a topic
 * @returns {Promise<void>}
 */
async function createAnswer(){
    const content = quillEditor.container.firstChild.innerHTML;
    const matchRegexUrl = new RegExp("([0-9]+)");
    const regexExec = matchRegexUrl.exec(window.location.href);
    const topicId = regexExec[0];
    const body = new FormData();

    document.getElementById("errorTopicAnswer").innerHTML = "";

    body.append("topicId",topicId);
    body.append("content",content);

    const req = await fetch("/post/answer/topic",{
        method:"POST",
        headers: {
            "Accept":"application/json"
        },
        body
    })

    const res = await req.json();

    if (req.status === 400){
        Object.keys(res.message).forEach((e) => {
            let li = document.createElement("li");
            li.className = "red-text";
            li.append(res.message[e]);
            document.getElementById("errorTopicAnswer").append(li);
        })
    }else{
        location.reload();
    }


}

/**
 * Function used to set the modal to delete an answer
 * @param id
 */
function beforeDeleteAnswerTopic(id){

    deleteAnswerTitle.textContent = "Supprimer la réponse?";
    deleteAnswerId = id;
}

/**
 * Function used to delete an answer
 * @returns {Promise<void>}
 */
async function deleteAnswerTopic(){
    const body = new FormData();
    body.append("id",deleteAnswerId);


    const req = await fetch("/post/delete/answer", {
        method : "POST",
        headers : {
            "Acccept": "application/json"
        },
        body
    })

    const res = await req.json();


    if (req.status === 400) {
        Object.keys(res.message).forEach((e) => {
            M.toast(res.message[e])
        })
    } else {
        location.reload();
    }
}

